#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

int main(int argc, char* argv[]){
    char buf[1024];
    int ret;
    int i, len, sockfd;
    struct sockaddr_in dest_addr;

    sockfd = socket(AF_INET ,SOCK_DGRAM, 0);
    if(sockfd == -1){
        printf("%s\n", strerror(errno));
        return -1;
    }

    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.sin_family=AF_INET;
    dest_addr.sin_port = htons(10000);
    dest_addr.sin_addr.s_addr=inet_addr("192.168.2.8");


    ret = connect(sockfd,(struct sockaddr *)&dest_addr, sizeof(dest_addr));
    if(ret != 0){
        printf("%s\n", strerror(errno));
        return -1;
    }

    i = 0;
    len =  sizeof(buf);
    while(i<10000)
    {
        i++;
        int ret = sendto(sockfd, buf, len, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (ret < 0)
        {
            perror("write error");
            break;
        }

    }
    printf("send number:%d\n",i);

    close(sockfd);

    return 0;
}
