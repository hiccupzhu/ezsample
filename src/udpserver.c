#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

int main(int argc, char* argv[]){
    char buf[1024];
    int i, sockfd, ret;
    struct sockaddr_in dest_addr;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1){
        printf("%s\n", strerror(errno));
        return -1;
    }

    bzero(&dest_addr,sizeof(dest_addr));
    socklen_t addrlen;
    dest_addr.sin_family=AF_INET;
    dest_addr.sin_port=htons(10000);
    dest_addr.sin_addr.s_addr=INADDR_ANY;
    ret = bind(sockfd,(struct sockaddr*)&dest_addr, sizeof(dest_addr));
    if(ret != 0){
        printf("%s\n", strerror(errno));
        return -1;
    }

    i = 0;
    while (1)
    {
        i++;
        int ret = recvfrom(sockfd, buf, 1024, 0,(struct sockaddr*) &dest_addr, &addrlen);
        if(ret < 0)
        {
            perror("recvfrom");
            break;
        }

        printf ("%s,%d\n", inet_ntoa (dest_addr.sin_addr), i);

    }
    printf ("%s,%d\n", inet_ntoa (dest_addr.sin_addr), i);

    close(sockfd);

    return 0;
}
