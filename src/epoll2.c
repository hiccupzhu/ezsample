/*
 * epoll.c
 *
 *  Created on: May 9, 2013
 *      Author: szhu
 */

#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include "gdef.h"


#define MAX_EVENTS 500
typedef void(*MEPOLL_CALL_BACK)(struct epoll_event *ev);

////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _mepctx_t{
    int epoll_fd;
}mepctx_t;

typedef struct _epool_private_t
{
    MEPOLL_CALL_BACK call_back;
    mepctx_t *ctx;
    int  fd;
    int  status; // 1: in epoll wait list, 0 not in
    long last_active; // last active time
}epool_private_t;



////////////////////////////////////////////////////////////////////////////////////////////

//int g_epollFd;
//epool_private_t g_mevents[MAX_EVENTS+1]; // g_mevents[MAX_EVENTS] is used by listen fd

////////////////////////////////////////////////////////////////////////////////////////////

static void     epoll_event_in_cb   (struct epoll_event *ev);
static void     epoll_event_out_cb  (struct epoll_event *ev);
static void     epoll_aceept_conn_cb(struct epoll_event *ev);
static void     epoll_event_hup_cb  (struct epoll_event *ev);

static void     mepoll_add(mepctx_t *ctx, int fd, int events, MEPOLL_CALL_BACK call_back);
static void     mepoll_del(mepctx_t *ctx, struct epoll_event *ev);

static int      fd_read(int fd, char* data, int len);
static int      fd_write(int fd, char* data, int len);

static const char* epoll_event_to_string(enum EPOLL_EVENTS events);

////////////////////////////////////////////////////////////////////////////////////////////


// accept new connections from clients
static void
epoll_aceept_conn_cb(struct epoll_event *ev)
{
    struct sockaddr_in sin;
    epool_private_t *prv = ev->data.ptr;
    mepctx_t *ctx = prv->ctx;
    socklen_t len = sizeof(struct sockaddr_in);
    int nfd, i;
    // accept
    if((nfd = accept(prv->fd, (struct sockaddr*)&sin, &len)) == -1)
    {
        if(errno != EAGAIN && errno != EINTR)
        {
        }
        av_print("accept, %s\n", strerror(errno));

        return;
    }
    do
    {
        // set nonblocking
        int iret = 0;
        if((iret = fcntl(nfd, F_SETFL, O_NONBLOCK)) < 0)
        {
            av_print("fcntl nonblocking failed:%d", iret);
            break;
        }
        // add a read events for receive data
        mepoll_add(ctx, nfd, EPOLLIN,  epoll_event_in_cb);
        mepoll_add(ctx, nfd, EPOLLHUP, epoll_event_hup_cb);

//        mepoll_add(ctx, nfd, EPOLLOUT, epoll_event_out_cb);
    }while(0);

    av_print("new conn[%s:%d][time:%d], pos[%d]\n",
            inet_ntoa(sin.sin_addr),
            ntohs(sin.sin_port),
            prv->last_active, i);
}

static void
epoll_event_hup_cb(struct epoll_event *ev)
{
    epool_private_t *prv = ev->data.ptr;
    av_print("@@@@@ %s\n", epoll_event_to_string(ev->events));

    mepoll_del(prv->ctx, ev);
}


// receive data
static void
epoll_event_in_cb(struct epoll_event *ev)
{
    epool_private_t *prv = ev->data.ptr;
    mepctx_t *ctx = prv->ctx;
    int len;
    char buff[1024];
    // receive data
#if 0
    len = recv(prv->fd, buff, sizeof(buff), 0);
#else
    len = fd_read(prv->fd, buff, sizeof(buff));
#endif
    //    mepoll_del(ctx, ev);
    if(len > 0)
    {
        buff[len] = '\0';
        av_print("FD[%d]:%s\n", prv->fd, buff);
        // change to send events
//        mepoll_add(ctx, prv->fd, EPOLLOUT, epoll_event_out_cb);
    }
    else if(len == 0)
    {
        mepoll_del(prv->ctx, ev);

        av_print("[fd=%d], closed gracefully.\n", prv->fd);
    }
    else
    {
        mepoll_del(prv->ctx, ev);
        av_print("fd_read[fd=%d] error[%d]:%s\n", prv->fd, errno, strerror(errno));
    }
}
// send data
static void
epoll_event_out_cb(struct epoll_event *ev)
{
    epool_private_t *prv = ev->data.ptr;
    mepctx_t *ctx = prv->ctx;
    int len;
    char buff[1024];


    sprintf(buff, "***************Hello***************");
    // send data
    len = send(prv->fd, buff, strlen(buff), 0);
    if(len > 0)
    {
        // change to receive events
//        mepoll_del(ctx, ev);
//        mepoll_add(ctx, prv->fd, EPOLLIN, epoll_event_in_cb);
    }
    else
    {
        mepoll_del(ctx, ev);
        av_print("send[fd=%d] error[%s]\n", prv->fd, strerror(errno));
    }
}


int main(int argc, char **argv)
{
    int i;
    mepctx_t *ctx;
    unsigned short port = 3000; // default port
    if(argc == 2){
        port = atoi(argv[1]);
    }


    ctx = calloc(1, sizeof(mepctx_t));
    // create epoll
    ctx->epoll_fd = epoll_create(MAX_EVENTS);
    if(ctx->epoll_fd <= 0){
        av_print("create epoll failed.%d\n", ctx->epoll_fd);
        exit(1);
    }

    // create & bind listen socket, and add to epoll, set non-blocking
    {
        int listenfd = socket(AF_INET, SOCK_STREAM, 0);
        fcntl(listenfd, F_SETFL, O_NONBLOCK); // set non-blocking
        av_print("server listen fd=%d\n", listenfd);

        int opt = SO_REUSEADDR;
        setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

        // add listen socket
        mepoll_add(ctx, listenfd, EPOLLIN , epoll_aceept_conn_cb);
        // bind & listen
        struct sockaddr_in sin;
        bzero(&sin, sizeof(sin));
        sin.sin_family = AF_INET;
        sin.sin_addr.s_addr = INADDR_ANY;
        sin.sin_port = htons(port);

        if(bind(listenfd, (const struct sockaddr*)&sin, sizeof(sin)) != 0){
            av_print("*** ERROR: Bind error\n");
            exit(1);
        }
        if(listen(listenfd, 5) != 0){
            av_print("*** ERROR: Listen FAILED.\n");
            exit(1);
        }

        av_print("server running:port[%d]\n", port);
    }


    // events loop
    struct epoll_event ep_events[MAX_EVENTS];
    while(1){
#if 0
        // a simple timeout check here, every time 100, better to use a mini-heap, and add timer events
        long now = time(NULL);
        for(i = 0; i < 100; i++) // doesn't check listen fd
        {
            if(i == MAX_EVENTS) i = 0; // recycle
            if(ctx->mevents[i].status != 1) continue;
            long duration = now - ctx->mevents[i].last_active;
            if(duration >= 60) // 60s timeout
            {
                close(ctx->mevents[i].fd);
                av_print("[fd=%d] timeout[%lld--%lld].\n", ctx->mevents[i].fd, ctx->mevents[i].last_active, now);
                mepoll_del(ctx, &ctx->mevents[i]);
            }
        }
#endif
        // wait for events to happen
        int fds = epoll_wait(ctx->epoll_fd, ep_events, MAX_EVENTS, 1000);
        if(fds < 0){
            av_print("epoll_wait error, exit\n");
            break;
        }

        for(i = 0; i < fds; i++){
            epool_private_t *prv = ep_events[i].data.ptr;
            av_print("### [%d/%d] RCV-EVENTS[%s] FD[%d] prv=0x%X\n",
                    i, fds, epoll_event_to_string(ep_events[i].events), ep_events[i].data.fd, prv);
            if(ep_events[i].events & EPOLLIN ) // read events
            {
                prv->call_back(&ep_events[i]);
            }
            if(ep_events[i].events & EPOLLOUT) // write events
            {
                prv->call_back(&ep_events[i]);
            }
        }
    }

    SAFE_FREE(ctx);
    // free resource
    return 0;
}


// add/mod an events to epoll
static void
mepoll_add(mepctx_t *ctx, int fd, int events, MEPOLL_CALL_BACK call_back)
{
    struct epoll_event ev = {0, {0}};
    int op;

    epool_private_t *prv = calloc(1, sizeof(epool_private_t));

    av_print("prv=0x%X %s\n", prv, epoll_event_to_string(events));
    ev.data.ptr = prv;
    ev.events = events;

    prv->fd = fd;
    prv->call_back = call_back;
    prv->ctx = ctx;

    if(prv->status == 1){
        op = EPOLL_CTL_MOD;
    }
    else{
        op = EPOLL_CTL_ADD;
        prv->status = 1;
    }
    if(epoll_ctl(ctx->epoll_fd, op, fd, &ev) < 0)
        av_print("Event Add failed[fd=%d], evnets[%0X]\n", fd, ev.events);
    else
        av_print("Event Add OK[fd=%d], op=%d, evnets[%0X]\n", fd, op, ev.events);
}
// delete an events from epoll
static void
mepoll_del(mepctx_t *ctx, struct epoll_event *_ev)
{
    struct epoll_event ev = {0, {0}};
    epool_private_t *prv = _ev->data.ptr;
    if(prv->status != 1) return;
    prv->status = 0;

    SAFE_CLOSE(prv->fd);

    epoll_ctl(ctx->epoll_fd, EPOLL_CTL_DEL, prv->fd, &ev);

    SAFE_FREE(prv);
}

static int
fd_read(int fd, char* data, int len)
{
    int nb;
    int len1 = len;
    while(len1 > 0){
        nb = read(fd, data, len1);
        av_print("nb=%d\n", nb);
        if(nb <= 0){
            break;
        }else{
            len1 -= nb;
        }
    }

    if((len - len1) > 0)
        return (len - len1);

    return nb;
}

static int
fd_write(int fd, char* data, int len)
{
    int nb;
    int len1 = len;
    while(len1 > 0){
        nb = write(fd, data + (len - len1), len1);
        if(nb <= 0)
            break;
        else
            len1 -= nb;
    }

    if((len - len1) > 0)
        return (len - len1);

    return nb;
}

static const char*
epoll_event_to_string(enum EPOLL_EVENTS events)
{
    static char name[1024];
    static const struct{
        enum EPOLL_EVENTS events;
        const char* name;
    } ep_tbales[] = {
        {EPOLLIN,     "EPOLLIN"},
        {EPOLLPRI,    "EPOLLPRI"},
        {EPOLLOUT,    "EPOLLOUT"},
        {EPOLLRDNORM, "EPOLLRDNORM"},
        {EPOLLRDBAND, "EPOLLRDBAND"},
        {EPOLLWRNORM, "EPOLLWRNORM"},
        {EPOLLWRBAND, "EPOLLWRBAND"},
        {EPOLLMSG,    "EPOLLMSG"},
        {EPOLLERR,    "EPOLLERR"},
        {EPOLLHUP,    "EPOLLHUP"},
        {EPOLLRDHUP,  "EPOLLRDHUP"},
        {EPOLLONESHOT,"EPOLLONESHOT"},
        {EPOLLET,     "EPOLLET"},
        {0, NULL}
    };
    int i;

    name[0] = 0;
    for(i = 0; ep_tbales[i].name != NULL; i ++){
        if(ep_tbales[i].events & events){
            strcat(name, ":");
            strcat(name, ep_tbales[i].name);
        }
    }
    if(name[0] == 0){
        strcpy(name, "null");
    }
    return name;
}
