#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    size_t len;
    int sk, talk, ret;
    char buff[1024] = {'\0'};
    struct sockaddr_in server, client;

    sk = socket(AF_INET, SOCK_STREAM, 0);
    if(sk == -1){
        printf("%s\n", strerror(errno));
        return -1;
    }

    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(3000);
    server.sin_addr.s_addr = htonl(INADDR_ANY);

    ret = bind(sk, (struct sockaddr*)&server, sizeof(server));
    if(ret != 0){
        printf("%s\n", strerror(errno));
        return -1;
    }

    ret = listen(sk, 5);
    if(ret != 0){
        printf("%s\n", strerror(errno));
        return -1;
    }

    bzero(&client, sizeof(client));
    len = sizeof(client);
    talk = accept(sk, (struct sockaddr*)&client, (socklen_t *)&len);
    if(talk == -1){
        printf("%s\n", strerror(errno));
        return -1;
    }

    ret = send(talk, "Hello", strlen("Hello") + 1, 0);
    if(ret < 0){
        printf("send error\n");
        return -1;
    }else{
        printf("the number sent %d bytes\n", ret);
    }

    ret = recv(talk, buff, sizeof(buff), 0);
    if(ret < 0){
        printf("send error\n");
        return -1;
    }else{
        printf("the number read %d bytes\n", ret);
    }
    printf("%s\n", buff);

    close(talk);
    close(sk);
    return 0;
}
