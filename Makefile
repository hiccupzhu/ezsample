CC=gcc
CFLAGS=-g
LDFLAGS=
#INCLUDE=
#LIBS= 

EXAMPLES += udpserver udpclient tcpserver tcpserver2 tcpclient select epoll epoll2
EXAMPLES += binarytree
BINDIR=./bin
OBJSDIR=./objs
VPATH=./src

EXAMPLES:=$(addprefix $(BINDIR)/,$(EXAMPLES))
#OBJS=$(addprefix $(BINDIR)/,$(addsuffix .o,$(EXAMPLES)))

#all:
#	mkdir -p $(BINDIR)
#	for i in $(EXAMPLES) ; do \
#		$(CC) $(CFLAGS) -o $(BINDIR)/$$i "$$i.c"; \
#	done

#$(warning "=====", $(EXAMPLES))

.SECONDARY: $(OBJECTS)

all: mk_dir $(EXAMPLES)

$(BINDIR)/%: $(OBJSDIR)/%.o
	$(CC) $< $(LDFLAGS) -o $@

$(OBJSDIR)/%.o: %.c
	$(CC) -c $< $(CFLAGS) -o $@

mk_dir:
	mkdir -p $(BINDIR)
	mkdir -p $(OBJSDIR)

clean:
	rm -rf $(BINDIR) $(EXAMPLES) $(OBJSDIR)

